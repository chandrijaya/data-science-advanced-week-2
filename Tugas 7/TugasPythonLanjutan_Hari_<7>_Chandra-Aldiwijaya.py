#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 17:14:07 2020

@author: bokab
"""
import sqlite3

def read(connection):
    #connection = sqlite3.connect(db)

    crud_query ='''SELECT * FROM indo_islands'''
    
    cursor = connection.cursor()
    cursor.execute(crud_query)
    get_sql =  cursor.fetchall()
    cursor.close()
    connection.close()
    return get_sql

def function():
    import pandas as pd
    
    
    df = pd.read_csv("pulau_indonesia.csv")
    df.fillna(value=0, inplace=True)
    
    db_file = "chandra.aldiwijaya.694.db"
    connection = sqlite3.connect(db_file)
    cursor = connection.cursor()
    
    create_indo_islands_table = '''CREATE TABLE IF NOT EXISTS indo_islands (
                                 id INTEGER PRIMARY KEY AUTOINCREMENT,
                                 provinsi TEXT NOT NULL,
                                 named integer NOT NULL,
                                 unnamed integer NOT NULL,
                                 total integer NOT NULL);'''
    
    crud_query = '''insert into indo_islands (provinsi, named, unnamed, total) 
                    values (?,?,?,?);'''
    
    cursor.execute(create_indo_islands_table)
    for index, row in df.iterrows():
        a, b, c, d, e = row
        data = (b, c, d, e)
        cursor.execute(crud_query,data)
    
    connection.commit()
    
    
    return connection

if __name__ == "__main__":
    cek = function()
    cek2 = read(cek)
    print(cek2)