#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 19:16:41 2020

@author: bokab
"""




def start():
    import requests
    from bs4 import BeautifulSoup
    import pandas as pd
    
    req = requests.get("https://en.wikipedia.org/wiki/Citra_Award_for_Best_Film")
    html = BeautifulSoup(req.content, 'html5lib')
    tableList = html.findAll("table", {"class":"wikitable"})
    tableBody = tableList[-1]("tr")
    tableHead = tableBody.pop(0)
    headData = [(header.get_text()).replace('\n','') for header in tableHead(["td", "th"])]
    bodyData = []
    for row in tableBody:
        temp = []
        for cell in row(["td", "th"]):
            for span in cell('span'):
                span.decompose()
            
            temp.append((cell.get_text()).replace('\n',''))
        
        bodyData.append(temp)
    
        
    df = pd.DataFrame(bodyData, columns=headData)
    cols = ['Film', 'Production company(s)', 'Director(s)']
    df = df[cols]
    print(df)

def start2():
    import requests
    from bs4 import BeautifulSoup
    import sqlite3
    
    req = requests.get("http://geospasial.bnpb.go.id/pantauanbencana/data/datagempaall.php")
    html = BeautifulSoup(req.content, 'html5lib')
    tableList = html.find("table", {"class":"myClass"})
    tableBody = tableList("tr")
    tableBody.pop(0)
    bodyData = []
    for row in tableBody:
        temp = []
        for no, cell in enumerate(row(["td", "th"])):
            for span in cell('span'):
                span.decompose()
            cek = (cell.get_text()).replace('\n','')
            cek = cek.replace(' SR', '')
            cek = cek.replace(' WIB', '')
            if (no == 3):
                cek = cek.split(',')
                for value in cek:
                    temp.append(float(value))
                
            elif (no == 5):
                cek = cek.replace(' km', '')
                cek = cek.replace(' Km', '')
                temp.append(float(cek))
                
            else:
                try:
                    temp.append(int(cek))
                
                except:
                    try:
                        temp.append(float(cek))
                
                    except:
                        temp.append(cek)
        
        temp.pop(0)
        bodyData.append(temp)
    
    db_file = "chandra.aldiwijaya.694.db"
    connection = sqlite3.connect(db_file)
    cursor = connection.cursor()
    
    create_indo_islands_table = '''CREATE TABLE IF NOT EXISTS indo_earthquakes (
                                 id INTEGER PRIMARY KEY AUTOINCREMENT,
                                 tanggal timestamp NOT NULL,
                                 waktu timestamp NOT NULL,
                                 bujur integer NOT NULL,
                                 lintang integer NOT NULL,
                                 magnitude integer NOT NULL,
                                 kedalaman integer NOT NULL,
                                 lokasi text NOT NULL);'''
    
    crud_query = '''insert into indo_earthquakes (tanggal, waktu, bujur, lintang, magnitude, kedalaman, lokasi) 
                    values (?,?,?,?,?,?,?);'''
    
    cursor.execute(create_indo_islands_table)
    for row in bodyData:
        a, b, c, d, e, f, g, h, i = row
        data = (a, b, c, d, e, f, g)
        cursor.execute(crud_query,data)
    
    connection.commit()
    cursor.close()
    connection.close()
    
def start3():
    import sqlite3
    import pandas as pd
    import matplotlib.pyplot as plt
    
    connection = sqlite3.connect("chandra.aldiwijaya.694.db")

    crud_query ='''SELECT * FROM indo_earthquakes'''
    
    cursor = connection.cursor()
    cursor.execute(crud_query)
    get_sql =  cursor.fetchall()
    cursor.close()
    connection.close()
    
    header = ['no', 'tanggal', 'waktu', 'bujur', 'lintang', 'magnitude', 'kedalaman', 'lokasi']
    df = pd.DataFrame(get_sql, columns=header)
    #df = df[df.lintang != 38.49]
    print("Kedalaman rata-rata gempa sebesar " + str(df.kedalaman.mean()) + " km")
    print("Dengan kedalaman maksimum sebesar " + str(df.kedalaman.max()) + " km")
    print("Magnitudo rata-rata gempa sebesar " + str(df.magnitude.mean()) + " SR")
    print("Dengan magnitudo maksimum sebesar " + str(df.magnitude.max()) + " SR")
    
    fig1, ax1 = plt.subplots(figsize=(13,6))
    
    ax1.scatter(df.bujur, df.lintang, marker='.', s=(2**df.magnitude)*1.5)
    
    ax1.set_title('Peta Sebaran Gempa di Indonesia')
    ax1.set_ylabel('Garing Lintang')
    ax1.set_xlabel('Garis Bujur')
    ax1.set_xlim(95, 141)
    ax1.set_ylim(-11, 6)
    
    #fig1.savefig('chandra.aldiwijaya.694.png')
    
    fig2, ax2 = plt.subplots(figsize=(6,6))
    
    ax2.scatter(df.magnitude, df.kedalaman, marker='.')
    
    ax2.set_title('Perbandingan Magnitude Terhadap Kedalaman Gempa di Indonesia')
    ax2.set_ylabel('Kedalaman')
    ax2.set_xlabel('Magnitude')
    
    #fig2.savefig('chandra.aldiwijaya.694.png')
    
    plt.show()
    
    

if __name__ == "__main__":
    start()
    start2()
    start3()
