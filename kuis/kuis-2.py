#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 19:16:41 2020

@author: bokab
"""




class webTableScrap:
    html = ""; headData = []; bodyData = []
    
    def __init__(self, url):
        import requests
        from bs4 import BeautifulSoup
        
        req = requests.get(url)
        self.html = BeautifulSoup(req.content, 'html5lib')
        self.__genTable()
    
    def __rowCheck(self, row):
        temp = []
        for cell in row:
            for span in cell('span'):
                span.decompose()
                
            if not cell.has_attr("colspan"):
                content = (cell.get_text()).replace('\n','')
                content = content.replace('−', '-')
                try:
                    temp.append(int(content.replace(',', '')))
                
                except:
                    try:
                        temp.append(float(content))
                    
                    except:
                        temp.append(str(content))
                

            else:
                if cell.has_attr("colspan"):
                    count = 0
                    while (count < int(cell["colspan"])):
                        temp.append(None)
                        count += 1
                
                else:
                    temp.append(None)
                

        return temp  
    
    def __genTable(self):
        tableList = self.html.findAll("table", {"class":"wikitable"})
        tableBody = tableList[-1]("tr")
        tableHead = (tableBody.pop(0))
        self.headData = [(header.get_text()).replace('\n','') for header in tableHead(["td", "th"])]
        for row in tableBody:
            temp = []
            for cell in row(["td", "th"]):
                for span in cell('span'):
                    span.decompose()
                
                temp.append((cell.get_text()).replace('\n',''))
            
            self.bodyData.append(temp)

    def dataFrame(self):
        import pandas as pd
        
        df = pd.DataFrame(self.bodyData, columns=self.headData)
        return df
    
    def genXLSX(self, targetName = "webTable.xlsx"):
        from openpyxl import Workbook
        
        wb = Workbook()
        ws = wb.active
        
        ws.append(self.headData)
        for row in self.bodyData:
            ws.append(row)
        
        wb.save(targetName)

    
def start():
    run = webTableScrap("https://en.wikipedia.org/wiki/Citra_Award_for_Best_Film")
    #run.genXLSX("chandra.aldiwijaya.694.xlsx")
    dataframe = run.dataFrame()
    print(dataframe)

if __name__ == "__main__":
    start()