#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 17:26:15 2020

@author: bokab
"""

import pandas as pd
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup


def start():
    url = "https://www1.gogoanime.movie/"
    
    req = Request(url , headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    html = BeautifulSoup(webpage, 'html.parser')
    
    div = html.find("div", {"class":"last_episodes"})
    
    title = div.find_all("p", {"class":"name"})
    la_ep = div.find_all("p", {"class":"episode"})
    
    data = []
    for a, b in zip(title, la_ep):
        print(a.a)
        data.append([(a.a.contents)[0],(b.contents)[0]])
    
    df = pd.DataFrame(data, columns=["Judul Anime", "Episode Terakhir"])
    print(df)


if __name__ == "__main__":
    start()