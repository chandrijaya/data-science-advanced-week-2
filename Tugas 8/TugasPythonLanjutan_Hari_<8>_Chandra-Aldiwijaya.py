#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 17:33:30 2020

@author: bokab
"""

import pandas as pd
import matplotlib.pyplot as plt


def start():
    df = pd.read_csv("INDODAPOERData.csv")
    df = df.loc[df['Indicator Name'] == "Total GDP excluding Oil and Gas (in IDR Million), Current Price"]
    df = df.loc[(df['Country Name'] == 'Bandung Barat, Kab.') | 
                (df['Country Name'] == 'Bandung, Kab.') | 
                (df['Country Name'] == 'Cimahi, Kota') |
                (df['Country Name'] == 'Bandung, Kota')]
    
    
    cols = ['Country Name', '2001', '2002', '2003', '2004', '2005', 
             '2006', '2007', '2008', '2009', '2010', '2011']
    
    df = df[cols].transpose()
    df.columns = df.iloc[0]
    df = df.drop(df.index[0])
    df.fillna(value=0, inplace=True)

    x = range(2001, 2012)

    fig, ax = plt.subplots(figsize=(13,6))
    
    ax.plot(x, df['Bandung, Kota'], marker='.', label='Bandung, Kota')
    ax.plot(x, df['Bandung, Kab.'], marker='*', label='Bandung, Kab.')
    ax.plot(x, df['Bandung Barat, Kab.'], marker='x', label='Bandung Barat, Kab.')
    ax.plot(x, df['Cimahi, Kota'], marker='p', label='Cimahi, Kota')
    
    ax.set_title('Total GDP excluding Oil and Gas, Current Price')
    ax.set_ylabel('Total GDP (IDR Million)')
    ax.set_xlabel('Year')
    ax.legend(loc=("upper left"))
    
    fig.savefig('chandra.aldiwijaya.694.png')
    
    plt.show()

    
if __name__ == "__main__":
    start()