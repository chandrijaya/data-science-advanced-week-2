SELECT a.name AS Nama_Artis,
	b.Title AS Nama_Album, 
	c.name AS Judul_Lagu
FROM tracks c
	JOIN albums b ON c.AlbumId = b.AlbumId
	JOIN artists a ON b.ArtistId = a.ArtistId
WHERE Nama_Artis = "Aerosmith" 
	OR (Nama_Artis = "AC/DC" 
		AND Nama_Album = "Let There Be Rock")