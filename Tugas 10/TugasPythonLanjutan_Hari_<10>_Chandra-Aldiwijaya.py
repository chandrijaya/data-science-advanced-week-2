#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 17:34:24 2020

@author: bokab
"""

class webTableScrap:
    html = ""; headData = []; bodyData = []
    
    def __init__(self, url):
        import requests
        from bs4 import BeautifulSoup
        
        req = requests.get(url)
        self.html = BeautifulSoup(req.content, 'html5lib')
        self.__genTable()
    
    def __rowCheck(self, row):
        temp = []
        for cell in row:
            for span in cell('span'):
                span.decompose()
                
            if not (cell.get_text() == '\xa0\n'):
                content = (cell.get_text()).replace('\n','')
                content = content.replace('−', '-')
                try:
                    temp.append(int(content.replace(',', '')))
                
                except:
                    try:
                        temp.append(float(content))
                    
                    except:
                        temp.append(str(content))
                

            else:
                if cell.has_attr("colspan"):
                    count = 0
                    while (count < int(cell["colspan"])):
                        temp.append(None)
                        count += 1
                
                else:
                    temp.append(None)
                

        return temp  
    
    def __genTable(self):
        tableBody = self.html.table('tr')
        tableHead = (tableBody.pop(0))

        self.headData = self.__rowCheck(tableHead('th'))
        
        for row in tableBody:
            self.bodyData.append(self.__rowCheck(row('td')))

    def dataFrame(self):
        import pandas as pd
        
        df = pd.DataFrame(self.bodyData, columns=self.headData)
        return df
    
    def genXLSX(self, targetName = "webTable.xlsx"):
        from openpyxl import Workbook
        
        wb = Workbook()
        ws = wb.active
        
        ws.append(self.headData)
        for row in self.bodyData:
            ws.append(row)
        
        wb.save(targetName)

    
def start():
    run = webTableScrap("https://en.wikipedia.org/wiki/List_of_brightest_stars")
    run.genXLSX("chandra.aldiwijaya.694.xlsx")
    dataframe = run.dataFrame()
    print(dataframe)

if __name__ == "__main__":
    start()