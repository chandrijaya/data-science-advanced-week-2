#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 17:34:24 2020

@author: bokab
"""

class webTableScrap:
    html = ""; headData = []; bodyData = []
    
    def __init__(self, url, ithTable=0):
        import requests
        from bs4 import BeautifulSoup
        
        req = requests.get(url)
        self.html = BeautifulSoup(req.content, 'html5lib')
        self.__genTable(ithTable)
    
    def __rowCheck(self, row, noRow=-1, removeSpan=False):
        value = []; rowSpan = []
        for noCell, cell in enumerate(row):
            if removeSpan:
                for span in cell('span'):
                    span.decompose()
                
            if not cell.has_attr("colspan"):
                content = (cell.get_text()).replace('\n','')
                content = content.replace('−', '-')
                content = content.replace('\xa0', '')            
                try:
                     data = int(content.replace(',', ''))
                
                except:
                    try:
                        data = float(content)
                    
                    except:
                        data = str(content)
                    
                value.append(data)
                
                if cell.has_attr("rowspan"):
                    
                    rowSpan.append([noRow, noCell, int(cell["rowspan"]), data])
                

            else:
                if cell.has_attr("colspan"):
                    count = 0
                    while (count < int(cell["colspan"])):
                        if (count == 0):
                            value.append((cell.get_text()).replace('\n',''))
                        
                        else:    
                            value.append(None)
                        
                        count += 1
                
                else:
                    value.append(None)
                

        if (noRow == -1):
            return value
            
        return value, rowSpan
    
    def __genTable(self, i):
        tableList = self.html.findAll("table", {"class":"wikitable"})
        tableBody = tableList[i]("tr")
        tableHead = (tableBody.pop(0))
        self.headData = [header.get_text() for header in tableHead(["td", "th"])]
        rowSpan = []
        for no, row in enumerate(tableBody):
            value, checkRowSpan = self.__rowCheck(row(["td", "th"]), no)
            self.bodyData.append(value)
            if checkRowSpan:
                for check in checkRowSpan:
                    rowSpan.append(check)
        
        if rowSpan:
            for value in rowSpan:
                #print(value)
                # tr value of rowspan in present in 1th place in results
                for index in range(1, value[2]):
                    #- Add value in next tr.
                    self.bodyData[value[0]+index].insert(value[1], value[3])
        

    def dataFrame(self):
        import pandas as pd
        
        df = pd.DataFrame(self.bodyData, columns=self.headData)
        return df
    
    def genXLSX(self, targetName = "webTable.xlsx"):
        from openpyxl import Workbook
        
        wb = Workbook()
        ws = wb.active
        
        ws.append(self.headData)
        for row in self.bodyData:
            ws.append(row)
        
        wb.save(targetName)

    
def start():
    run = webTableScrap("http://en.wikipedia.org/wiki/List_of_England_Test_cricket_records", 20)
    run.genXLSX("chandra.aldiwijaya.694.xlsx")
    dataframe = run.dataFrame()
    print(dataframe)

if __name__ == "__main__":
    start()